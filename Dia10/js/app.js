
function ShowCajaRoja(event) {
  event.preventDefault()
  $('.overlay').addClass('show')
}

function HideCajaRoja(event) {
  event.preventDefault()
  $('.overlay').removeClass('show')
}

// esperar el document ready
$(document).ready(function () {
  $('.toggle-caja-roja').click(ShowCajaRoja)
  $('.overlay').click(HideCajaRoja)
})
